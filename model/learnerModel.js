const mongoose = require('mongoose');
const learnerSchema = mongoose.Schema({
        name: {
            type: String,
            required: true
        },
        email: {
            type: String,
            required: true
        },
        password: {
            type: String,
            required: true
        },
        mobile: {
            type: String,
            required: true
        },
        accountUserId:{
            type: String,
            required: true
        }
    },
    {
        timestamps: true
    }
);

const learner = mongoose.model('learners', learnerSchema);

class learnerModel 
{
    add(data, callback) 
    {
        var currentDate = new Date();
        let learnerData = new learner({
            name: data.name,
            email: data.email,
            mobile: data.mobile,
            password: data.password,
            createdAt: currentDate,
            accountUserId: data.accountUserId,
        });
        return new Promise((resolve, reject) => {
            learnerData.save().then((result) => {
                resolve(result);
            }).catch((err) => {
                reject(err);
            })
        })
    }

    find(data, callback) {
        var getData = {}
        if(data != 0) { getData = {'_id':data} }
        //console.log(getData); return false;
        return new Promise((resolve, reject) => {
            learner.find(getData).then((result) => {
                resolve(result);
            }).catch((err) => {
                reject(err);
            })
        })
    }

    update(data, callback) {
        // console.log('fdsfds');
        var currentDate = new Date();
        let learnerData = {
            name: data.name,
            email: data.email,
            mobile: data.mobile,
            password: data.password,
            updatedAt: currentDate
        }
        return new Promise((resolve, reject) => {
            learner.findByIdAndUpdate({'_id':data.uid}, learnerData).then((result) => {
                resolve(result);
            }).catch((err) => {
                reject(err);
            })
        })
    }
}

module.exports = new learnerModel();
