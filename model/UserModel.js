
const mongoose = require('mongoose');
const userSchema = mongoose.Schema({
        name: {
            type: String,
            required: true
        },
        email: {
            type: String,
            required: true
        },
        password: {
            type: String,
            required: true
        },
        mobile: {
            type: String,
            required: true
        },
        userType: {
            type: String,
            enum: ['Facebook', 'Google', 'Local'],
            default: 'Local'
        },
        remoteUserId: {
            type: String,
            required: false
        },
        isVerified: {
            type: Boolean,
            default: false
        }
    },
    {
        timestamps: true
    }
);

const user = mongoose.model('users', userSchema);

class UserModel {
    add(data, callback) {
        var currentDate = new Date();
        let userData = new user({
            name: data.name,
            email: data.email,
            mobile: data.mobile,
            password: data.password,
            userType: data.userType,
            remoteUserId: data.remoteUserId,
            createdAt: currentDate
        });
        return new Promise((resolve, reject) => {
            userData.save().then((result) => {
                resolve(result);
            }).catch((err) => {
                reject(err);
            })
        })
    }

    find(data, callback) {
        var getData = {}
        if(data != 0) { getData = {'_id':data} }
        //console.log(getData); return false;
        return new Promise((resolve, reject) => {
            user.find(getData).then((result) => {
                resolve(result);
            }).catch((err) => {
                reject(err);
            })
        })
    }

    update(data, callback) {
        var currentDate = new Date();
        let userData = {
            name: data.name,
            email: data.email,
            mobile: data.mobile,
            password: data.password,
            updatedAt: currentDate
        }
        return new Promise((resolve, reject) => {
            user.findByIdAndUpdate({'_id':data.uid}, userData).then((result) => {
                resolve(result);
            }).catch((err) => {
                reject(err);
            })
        })
    }

}

module.exports = new UserModel();
