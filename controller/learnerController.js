
const learnerService = require('../service/learnerService');

class learnerController 
{
    /** Add learner to the database**/
    add(req, res) 
    {
        const responseResult = {}
        req.checkBody('name').isLength({ min: 3 }).withMessage('Name must contain minimum 3 alphabets');
        req.checkBody('mobile').isLength({ min: 10}).withMessage('Mobile number must be of 10 digit');
        req.checkBody("email", "email should be specified").isEmail();
        req.checkBody("password", "Password must contain minimum 6 alphnumeric characters").isLength({min: 6});

        const errors = req.validationErrors();
        if(errors)
        {
            responseResult.sucess = false;
            responseResult.message = "Please enter proper inputs";
            responseResult.errors = errors;
            res.status(400).send(responseResult)
        } else {
            let learnerData = {
                name: req.body.name,
                mobile: req.body.mobile,
                email: req.body.email,
                password: req.body.password,
                accountUserId: req.body.accountUserId
            }
            learnerService.add(learnerData).then((result) => 
            {
                if(result.code == 11000) 
                {
                    responseResult.sucess = false,
                    responseResult.message = "Duplicate record"
                } 
                else 
                {
                    responseResult.sucess = true,
                    responseResult.message = "Learner add sucessfully"
                }
                responseResult.result = result
                return res.status(201).send(responseResult)
            }).catch((errors) => {
                responseResult.success = false;
                responseResult.error = errors;
                return res.status(500).send(responseResult)
            })
        }
    }

    list(req, res) {
        const responseResult = {}; var uid = 0;
        if ( req.params.id ) { uid = req.params.id; }

        learnerService.find(uid).then((result) => {
            if(result) {
                responseResult.sucess = true,
                responseResult.message = "Learner Details",
                responseResult.result = result
            } else {
                responseResult.sucess = false,
                responseResult.message = "Not found Learner Details ",
                responseResult.result = result
            }
            return res.status(201).send(responseResult)
        }).catch((errors) => {
            responseResult.success = false;
            responseResult.error = errors;
            return res.status(500).send(responseResult)
        })
    }

    update(req, res) 
    {
        const responseResult = {};
        let learnerData = {
            uid: req.body.id,
            name: req.body.name,
            mobile: req.body.mobile,
            email: req.body.email,
            password: req.body.password,
        }
        learnerService.update(learnerData).then((result) => {
            if(result.code == 11000) {
                responseResult.sucess = false,
                responseResult.message = "Duplicate record"
            } else {
                responseResult.sucess = true,
                responseResult.message = "Learner updated sucessfully"
            }
            responseResult.result = result
            return res.status(201).send(responseResult)
        }).catch((errors) => {
            responseResult.success = false;
            responseResult.error = errors;
            return res.status(500).send(responseResult)
        })
    }
}
module.exports = new learnerController();






