const express = require('express');
const userRouter = express.Router();
const userController = require('../controller/UserController');
const learnerController= require('../controller/learnerController');
/**routes added  */
userRouter.post('/register', userController.add);
userRouter.get('/userinfo/:id', userController.list);
userRouter.get('/userlist', userController.list);
userRouter.put('/setuserinfo', userController.update);

/** learner api**/
userRouter.post('/learner', learnerController.add);
userRouter.put('/learnerUpdate', learnerController.update);
userRouter.get('/learnerList', learnerController.list);
/* end  learne api */
module.exports = userRouter;