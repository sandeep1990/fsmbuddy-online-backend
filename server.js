const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors')
const dotenv = require('dotenv').config();
// Configuring the database
const dbConfig = require('./config/dbconfig');
const mongoose = require('mongoose');
const userRoutes = require('./routes/UserRoutes');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger/swagger.json');
const expressValidator = require('express-validator');

// create express app
const app = express();
app.use(cors());
app.use(expressValidator());
app.use('/api', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));


app.use('/', userRoutes);

// define a simple route
app.get('/', (req, res) => {
    res.json({ "message": "Welcome to FSM Buddy" });
});

mongoose.Promise = global.Promise;

mongoose.connect(dbConfig.url, {
    useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to the database");
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    //process.exit();
});

// listen for requests
app.listen(3000, () => {
    console.log("Server is listening on port 3000");
});
