const sinon = require('sinon');
const chai = require('chai');
const userService = require('../service/UserService');
const userModel = require('../model/UserModel');
let sandbox;
let userModelStub;
describe("create customer detaials service", () => {
    before(function () {
        sandbox = sinon.createSandbox();
        customerModelStub = sandbox.stub(userModel, "add");
    });
    after(function () {
        userModel.add.restore();
    });

    it("Given user details should return proper json ", (done) => {
        let userData = {
            "name": "Suhas Mhatre",
            "mobile": "9870014036",
            "email": "suhas.mhatre@bridgelabz.com",
            "password": "suhas1234",
            "userType": "Local",
            "remoteUserId": ""
        }
        let expectedUserData = {
            "_id": "5e3d4ef47a31fd0d773d7867",
            "name": "Suhas Mhatre",
            "mobile": "9870014036",
            "email": "suhas.mhatre@bridgelabz.com",
        };
        customerModelStub.returns(Promise.resolve(expectedUserData));
        userService.add(customerDetails).then(data => chai.expect(data).to.be.eql(expectedUserData));
        done();
    });
})
