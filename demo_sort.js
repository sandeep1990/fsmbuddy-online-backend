var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/booksDB";

MongoClient.connect(url, function (err, db) {
    if (err) throw err;
    var dbo = db.db("booksDB");
     var options = {
         "sort": { "price": 1 }
    };
    dbo.collection("bookstores").find({}, options).toArray(function (err, result) {
        if (err) throw err;
        console.log("result----", result);
        db.close();
    });

});