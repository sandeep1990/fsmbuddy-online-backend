const learnerModel = require('../model/learnerModel');
class learnerService 
{
    /** Add learner details using promise **/
    add(data) 
    {
        return learnerModel.add(data)
        .then((result) => {
                return result
        })
        .catch((error) => {
                return error
        })
    }

    find(data)
    {
        return learnerModel.find(data)
        .then((result) => {
            return result
        })
        .catch((error) => {
            return error
        })
    }

    update(data){
        return learnerModel.update(data)
        .then((result) => {
            return result
        })
        .catch((error) => {
            return error
        })
    }
}
module.exports = new learnerService();





