const userModel = require('../model/UserModel');
class UserService {

    /** Add user details using promise **/
    add(data) {
        return userModel.add(data)
        .then((result) => {
                return result
        })
        .catch((error) => {
                return error
        })
    }

    find(data){
        return userModel.find(data)
        .then((result) => {
            return result
        })
        .catch((error) => {
            return error
        })
    }

    update(data){
        return userModel.update(data)
        .then((result) => {
            return result
        })
        .catch((error) => {
            return error
        })
    }
}
module.exports = new UserService();





